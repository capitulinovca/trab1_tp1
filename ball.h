/*! @file: ball.h
 *  @brief: Arquivo Header da classe bola
 *  @author: Vinicius Capitulino
 */

#ifndef __ball__
#define __ball__

#include "simulation.h"

/*! @brief Classe Ball
 * 
 *Esta classe implementa todos os métodos e declara todos os atributos de ball
 */
class Ball : public Simulation
{
public:
  // Constructors and member functions
  Ball() ;
  Ball(double x, double y);/// Construtor que  permite setar a posição inicial da bola
  void step(double dt);



  //Setter functions
  int setPosition(double x, double y); //seta posição inicial da bola

  //Getter functions
  void display();
  void displayX();
  void displayY();


protected:
  // Data members
  // Position and velocity of the ball
  double x ;
  double y ;
  double vx ;
  double vy ;

  // Mass and size of the ball
  double m ;
  double r ;

  // Gravity acceleration
  double g ;

  // Geometry of the box containing the ball
  double xmin ;
  double xmax ;
  double ymin ;
  double ymax ;
} ;

#endif /* defined(__ball__) */
