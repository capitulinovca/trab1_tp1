/** file: test-setposi.h
 ** brief: Testa a inserção de posição no código
 ** author: Vinicius Capitulino
 **/

 #include "ball.h"
 #include <iostream>
 #include <string>


 using namespace std;
 int main(int argc, char** argv)
 {
  double x,y;
  int res;
   Ball ball;
   const double dt = 1.0/30 ;
   cout<<"Crie um objeto da classe Ball"<<endl;
   do{
       cout<<"Digite as coordenas iniciais da bola (0 até 0.9)"<<endl;
       cout<<"x y: ";
       cin>>x;
       cin>>y;
       res=ball.setPosition(x,y);
   }while(res != 1);

   Ball(x,y);
   for (int i = 0 ; i < 100 ; ++i) {
     ball.step(dt) ;
     ball.display() ;
   }


  /*
   cout<<"DISPLAY X"<<endl<<endl;
   for (int i = 0 ; i < 100 ; ++i) { //Imprime só as coordenadas de x
     ball.step(dt) ;
     ball.displayX() ;
   }

   cout<<endl<<endl<<"DISPLAY Y"<<endl<<endl;
   for (int i = 0 ; i < 100 ; ++i) {//Imprime só as coordenadas de y
     ball.step(dt) ;
     ball.displayY() ;
   }
   */
   return 0 ;
 }
