﻿Departamento de Ciência da Computação - CIC  
Disciplina: Tecnicas de Programação 1  
Professora: Teófilo E. de Campos  
Código: 117889 – Período Letivo: 1/2017  
Aluno: Vinicius Capitulino de Andrade  Matrícula: 150151233  


# Trabalho 1
O trabalho 1 é todo baseado em um laboratório virtual que modela algumas interações de corpos, gerando gráficos e modelos em 2D em movimento.

### Diagrama de Classes do projeto

![classDiagr](https://gitlab.com/saat/trab1_tp1/raw/master/inherit_graph_1.png)


# Parte 1

### Bouncing ball
O programa simula o movimento de uma bola que respeita a equação de Euller.
O projeto foi implementado em C++ e contém os seguintes arquivos: ball.h, ball.cpp, simulation.h e os arquivos de teste que testam algo de acordo com a implementação.
A biblioteca iostream é a única biblioteca usada.  

O programa foi compilado com g++ 5.4.0, da seguinte forma: **_g++ ball.h ball.cpp test-setposi.cpp_**


##### Saída do teste com test-ball.cpp
0.01	-0.00877778  
0.02	-0.0175556  
0.03	-0.0263333  
0.04	-0.0351111  
0.05	-0.0438889  
0.06	-0.0526667  
0.07	-0.0614444  
0.08	-0.0702222  
0.09	-0.079  
0.1	-0.0877778  
0.11	-0.0965556  
0.12	-0.105333  
0.13	-0.114111  
0.14	-0.122889  
0.15	-0.131667  
0.16	-0.140444  
0.17	-0.149222  
0.18	-0.158  
0.19	-0.166778  
0.2	-0.175556  
0.21	-0.184333  
0.22	-0.193111  
0.23	-0.201889  
0.24	-0.210667  
0.25	-0.219444  
0.26	-0.228222  
0.27	-0.237  
0.28	-0.245778  
0.29	-0.254556  
0.3	-0.263333  
0.31	-0.272111  
0.32	-0.280889  
0.33	-0.289667  
0.34	-0.298444  
0.35	-0.307222  
0.36	-0.316  
0.37	-0.324778  
0.38	-0.333556  
0.39	-0.342333  
0.4	-0.351111  
0.41	-0.359889  
0.42	-0.368667  
0.43	-0.377444  
0.44	-0.386222  
0.45	-0.395  
0.46	-0.403778  
0.47	-0.412556  
0.48	-0.421333  
0.49	-0.430111  
0.5	-0.438889  
0.51	-0.447667  
0.52	-0.456444  
0.53	-0.465222  
0.54	-0.474  
0.55	-0.482778  
0.56	-0.491556  
0.57	-0.500333  
0.58	-0.509111  
0.59	-0.517889  
0.6	-0.526667  
0.61	-0.535444  
0.62	-0.544222  
0.63	-0.553  
0.64	-0.561778  
0.65	-0.570556  
0.66	-0.579333  
0.67	-0.588111  
0.68	-0.596889  
0.69	-0.605667  
0.7	-0.614444  
0.71	-0.623222  
0.72	-0.632  
0.73	-0.640778  
0.74	-0.649556  
0.75	-0.658333  
0.76	-0.667111  
0.77	-0.675889  
0.78	-0.684667  
0.79	-0.693444  
0.8	-0.702222  
0.81	-0.711  
0.82	-0.719778  
0.83	-0.728556  
0.84	-0.737333  
0.85	-0.746111  
0.86	-0.754889  
0.87	-0.763667  
0.88	-0.772444  
0.89	-0.781222  
0.89	-0.79  
0.88	-0.798778   
0.87	-0.807556  
0.86	-0.816333  
0.85	-0.825111  
0.84	-0.833889  
0.83	-0.842667  
0.82	-0.851444  
0.81	-0.860222  
0.8	-0.869  
0.79	-0.877778  





##### Gŕafico gerado pelo movimento da bola


![grafico](https://gitlab.com/saat/trab1_tp1/raw/master/grafico.png)




###### Arquivos da parte 1

**ball.h**  
Cria a classe ball, define seus métodos, variáveis e privilégios de acesso.

**ball.cpp**  
Inicializa um objeto "Ball" e implementa os métodos step, display e set.

**simulation.h**
Implementa a classe Simulation com os métodos para display e step.


**Arquivos de teste**  
São arquivos de teste que geram cenários usando os obejtos como base.

# Parte 2
### Spring-Mass

Esta simulação mostra o comportamente de duas massas ligadas por uma mola.

O programa foi compilado com g++ 5.4.0, da seguinte forma: **_g++ springmass.h springmass.cpp test-springmass.cpp_**

###### Arquivos da parte 2

**springmass.h**  

**springmass.cpp**  

**test-springmass.cpp**  

### Exemplo das saídas geradas na Task 15

### Trajetória das massas
(Inclua uma figura que permite a visualização da trajetória das massas. Essa figura pode ser gerada, por exemplo, em Octave ou Python.)
