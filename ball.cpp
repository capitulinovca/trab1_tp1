/*! @file: ball.cpp
 *  @author: Vinicius Capitulino
 *  @brief: Ball class - implementação da classe ball
 */

#include "ball.h"

#include <iostream>

using namespace std;

/*! @brief Inicializa um objeto "ball" da classe Ball
 *  @return Retorna um objeto ball com raio, tamanho x, tamanho y, velocidade x, velocidade y, aceleração da gravidade, massa e dimensões da caixa
 */
Ball::Ball()
: r(0.1), x(0), y(0),vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }


/*! @brief Inicializa um objeto "ball" da classe Ball, permitindo que o usuario defina a posicao inicial da bola.
 *  @return Retorna um objeto ball com raio, tamanho x, tamanho y, velocidade x, velocidade y, aceleração da gravidade, massa e dimensões da caixa
 */
Ball::Ball(double x, double y):r(0.1),vx(0.3), vy(-0.1), g(9.8), m(1), // Contrutor que instancia objeto e permite setar posição
xmin(-1), xmax(1), ymin(-1), ymax(1){
  this->x = x;
  this->y = y;
}

/*! @brief Modifica a posicao da bola
 *  @return Nova posicao da bola
 */

int Ball::setPosition(double x, double y) // Metodo de modificacao da posicao
{
      this->x=x;
      this->y=y;
      if(((x+r)>xmax) || ((y+r)>xmax)){
        return 2;
      }else return 1;
}

/*! @brief Modela o movimento da bola
 *  @return Atualiza a posicao da bola em funcao do tempo e da gravidade
 */

void Ball::step(double dt)
{
  double xp = x + vx * dt ;
  double yp = y + vy * dt - 0.5 * g * dt * dt ;
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
    vx = -vx ;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
    //vy = vy - g * dt ;
  } else {
    vy = -vy ;
  }
}


/*! @brief Imprime posicao da bola na tela
 *  @return Posicao x e y
 */

void Ball::display()
{
  
  cout<<x<<" "<<y<<endl ;
}
